package fedcorp.task1_ping_pong;

public class Blacksmith{
    private boolean isDone = false;
    private String sword;

    public synchronized void makeSword(int i){
            if (isDone) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        sword = "Меч №" + i;
        System.out.println("Виготовлений новий меч, його порядковий номер " + i);
        isDone=true;
        this.notify();

    }

    public synchronized void saleSword(){
            if (!isDone) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        System.out.println("Ви продали " + sword);
        sword = "Меча не існує";
        isDone=false;
        this.notify();
    }


}
