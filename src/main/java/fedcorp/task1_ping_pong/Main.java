package fedcorp.task1_ping_pong;

public class Main {

    public static void main(String[] args) {
        Blacksmith blacksmith = new Blacksmith();
        Thread thread_1 = new Thread("Blacksmith Thread"){
            @Override
            public void run(){
                for (int i = 1; i < 101; i++) {
                    blacksmith.makeSword(i);
                }
            }
        };
        Thread thread_2 = new Thread("Consumer Thread"){
            @Override
            public void run(){
                for (int i = 1; i < 101; i++) {
                    blacksmith.saleSword();
                }
            }
        };
        thread_1.start();
        thread_2.start();
    }
}
