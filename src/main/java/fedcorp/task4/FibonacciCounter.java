package fedcorp.task4;

import java.util.concurrent.Callable;

public class FibonacciCounter implements Callable {
    private int n;
    public FibonacciCounter(int n) {
        this.n=n;
    }
    @Override
    public Long call() throws Exception {
        return counter(n);
    }
    private synchronized long counter(int numbers){
        long[] feb = new long[numbers];
        long sum = 0;
        feb[0] = 0;
        feb[1] = 1;

        for(int i = 2; i < numbers; i++) {
            feb[i] = feb[i-1] + feb[i-2];
        }
        for(int i = 0; i< numbers; i++) {
            sum += feb[i];
        }
        return sum;
    }
}
