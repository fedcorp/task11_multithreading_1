package fedcorp.task5;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x=sc.nextInt();
        ScheduledExecutorService se = Executors.newScheduledThreadPool(5);
        long start = System.currentTimeMillis();

        for (int i = 0; i <x; i++) {
            se.schedule(new SomeTask(start), ((int)(Math.random()*10)),TimeUnit.SECONDS);

        }
            se.shutdown();
    }
    static class SomeTask implements Runnable{
        private long start;
        public SomeTask(long start) {
            this.start=start;
        }
        @Override
        public void run() {
            System.out.println("Task complited for "+ (new Date().getTime()- start + "ms."));
        }
    }
}


