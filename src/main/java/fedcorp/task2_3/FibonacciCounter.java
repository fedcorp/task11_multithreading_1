package fedcorp.task2_3;

public class FibonacciCounter implements Runnable {
    private int n;
    public FibonacciCounter(int n) {
        this.n=n;
    }

    @Override
    public void run() {
        counter(n);
        System.out.println();
    }
    private synchronized void counter(int numbers){
        System.out.println(Thread.currentThread().getName());
        long[] feb = new long[numbers];
        feb[0] = 0;
        feb[1] = 1;

        for(int i = 2; i < numbers; i++) {
            feb[i] = feb[i-1] + feb[i-2];
        }
        for(int i = 0; i< numbers; i++) {
            System.out.print(feb[i] + " ");
        }
    }
}
