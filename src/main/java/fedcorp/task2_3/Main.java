package fedcorp.task2_3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
//        main.executorOne();
//        main.executorTwo();
//        main.executorThree();
        main.executorFour();
    }

    void executorOne(){
        FibonacciCounter fb = new FibonacciCounter(25);
        ExecutorService es = Executors.newSingleThreadExecutor();
        for (int i = 0; i <10 ; i++) {
            es.submit(fb);
        }
        es.shutdown();
    }
    void executorTwo(){
        FibonacciCounter fb = new FibonacciCounter(25);
        ExecutorService es = Executors.newCachedThreadPool();
        for (int i = 0; i <10 ; i++) {
            es.submit(fb);
        }
        es.shutdown();
    }
    void executorThree(){
        FibonacciCounter fb = new FibonacciCounter(25);
        ExecutorService es = Executors.newFixedThreadPool(2);
        for (int i = 0; i <10 ; i++) {
            es.submit(fb);
        }
        es.shutdown();
    }

    void executorFour(){
        FibonacciCounter fb = new FibonacciCounter(25);
        ScheduledExecutorService es = Executors.newScheduledThreadPool(2);
            es.scheduleWithFixedDelay(fb, 0, 1, TimeUnit.SECONDS);
    }
}
