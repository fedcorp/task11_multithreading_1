package fedcorp.task6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        SynchroDemo sd = new SynchroDemo();
        SynchroDemoModified sdm = new SynchroDemoModified();
        ExecutorService service = Executors.newFixedThreadPool(3);
        //Після запуску програми бачимо, що медоти пряцюють послідовно, хоч і порядок запуску методів може бути різним.
        service.submit(()->sd.method1());
        service.submit(()->sd.method2());
        service.submit(()->sd.method3());

        //Методи синхронізовані різними об'єктами, а отже можуть виконуватися паралельно. Тому в результаті на консолі відображаються змішані дані трьох методів
        service.submit(()->sdm.method1());
        service.submit(()->sdm.method2());
        service.submit(()->sdm.method3());
        service.shutdown();
    }
}
