package fedcorp.task6;

public class SynchroDemo {

    public synchronized void method1(){
        for (int i = 0; i <=100 ; i++) {
            System.out.print("Method 1 "+i+" ");
        }
        System.out.println();
    }
    public synchronized void method2(){
        for (int i = 0; i <=100 ; i++) {
            System.out.print("Method 2 "+i+" ");
        }
        System.out.println();
    }
    public synchronized void method3(){
        for (int i = 0; i <=100 ; i++) {
            System.out.print("Method 3 "+i+" ");
        }
        System.out.println();
    }

}
