package fedcorp.task6;

public class SynchroDemoModified {

    private Object o1 = new Object();
    private Object o2 = new Object();
    private Object o3 = new Object();

    public void method1(){
        synchronized (o1) {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 1 " + i + " ");
            }
            System.out.println();
        }
    }
    public void method2(){
        synchronized (o2) {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 2 " + i + " ");
            }
            System.out.println();
        }
    }
    public void method3(){
        synchronized (o3) {
            for (int i = 0; i <= 100; i++) {
                System.out.print("Method 3 " + i + " ");
            }
            System.out.println();
        }
    }

}
