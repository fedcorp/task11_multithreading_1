package fedcorp.task7;

import java.io.*;

public class PipeCircle {
    PipedWriter pWriter;
    PipedReader pReader;
    Object o1 = new Object();
    Object o2 = new Object();


    public PipeCircle() {
        pWriter = new PipedWriter();
        try {
            pReader = new PipedReader(pWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromConsole() {
        synchronized (o1) {

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String tmp = "";
            do {
                try {
                    tmp = br.readLine();
                    pWriter.write(tmp.toCharArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } while (!tmp.equals(""));
        }
    }
//Існує проблема. Програма крашиться, коли PipedOutputStream перестає працювати і інпутстрім пробує зчитувати.
    public void displayOnConsole() {
        synchronized (o2) {
            int tmp = -1;
            do {
                try {
//                    if (pReader.ready()) {
                        tmp = pReader.read();
                        System.out.print((char) tmp);
//                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } while (tmp != -1);
        }
    }
}

